import React, { useState, Fragment } from "react";

import Header from "./components/Header/Header";
import PresentationTextApp from "./components/HomeCard/PresentationTextApp";
// import Card from "./components/UI/Card";
import DishList from "./components/Dish/DishList";
import CartContext from "./context/cart-context";

import './App.css';


function App(props) {
    const dish = [
        {
            id: 1,
            name: "Sushi",
            description: "Finest fish and veggies",
            price: 22.99,
            isInCart: false,
            amountOrdered: 0
        },
        {
            id: 2,
            name: "Schnitzel",
            description: "A german specialy !",
            price: 16.50,
            isInCart: false,
            amountOrdered: 0
        },
        {
            id: 3,
            name: "Barbecue Burger",
            description: "American, raw, meaty",
            price: 12.99,
            isInCart: false,
            amountOrdered: 0
        },
        {
            id: 4,
            name: "Green Bowl",
            description: "Healthy...and green...",
            price: 18.99,
            isInCart: false,
            amountOrdered: 0
        }
    ];

    const [dishInCart, setDishInCart] = useState([]);
    const [numberDishesInCart, setNumberDishesInCart] = useState(0);

    const dishAddToCart = (id, dishAmount) => {
        if (dishInCart.find(dish => dish.id === id)) {

            let updatedDishAmount = dishInCart.map(dish => {
                if (dish.id === id) {
                    return { ...dish, amountOrdered: dish.amountOrdered + dishAmount }; //gets everything that was already in item, and updates "done"
                }
                return dish; // else return unmodified item 
            });

            setDishInCart(updatedDishAmount); // set state to new object with updated list

        } else {

            const lastDishAddToCartArray = dish.filter(dish => dish.id === id);

            const [lastDishAddToCar] = lastDishAddToCartArray;
            lastDishAddToCar.amountOrdered = dishAmount;

            setDishInCart(prevDish => {
                return [lastDishAddToCar, ...prevDish];
            });
        }

        setNumberDishesInCart(prevState => prevState + dishAmount);
    };

    const incrementDish = (id) => {
        let updatedDishAmount = dishInCart.map(dish => {
            if (dish.id === id) {
                return { ...dish, amountOrdered: ++dish.amountOrdered }; //gets everything that was already in item, and updates "done"
            }
            return dish; // else return unmodified item
        });

        setDishInCart(updatedDishAmount); // set state to new object with updated list
        setNumberDishesInCart(prevState => ++prevState );
    };
    
    const decreaseDish = (id) => {
        let updatedDishAmount = dishInCart.map(dish => {
            if (dish.id === id) {
                return { ...dish, amountOrdered: --dish.amountOrdered }; //gets everything that was already in item, and updates "done"
            }
            return dish; // else return unmodified item
        });

        setDishInCart(updatedDishAmount); // set state to new object with updated list
        setNumberDishesInCart(prevState => --prevState );

        const amountOrderedAfterDecrease = dishInCart.find(dish => dish.id === id);
        removeDish(id, amountOrderedAfterDecrease);

    };

    const removeDish = (id, amountOrderedAfterDecrease) => {
        if (amountOrderedAfterDecrease.amountOrdered <= 0) {
            const updateDishInCart = dishInCart.filter(dish => dish.id !== id);
            setDishInCart(updateDishInCart);
        };
    }

    return (
        <Fragment>
                <CartContext.Provider
                    value={{
                        numberDishesInCart: numberDishesInCart,
                    }}
                >
                    <Header
                        dishInCartData={dishInCart}
                        incrementDish={incrementDish}
                        decreaseDish={decreaseDish}
                    />
                </CartContext.Provider>
                <PresentationTextApp />
                <DishList
                    dishData={dish}
                    onDishAddToCart={dishAddToCart}
                />
        </Fragment>

    );
}

export default App;
