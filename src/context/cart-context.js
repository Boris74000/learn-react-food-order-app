import React from "react";

const CartContex = React.createContext({
    numberDishesInCart: 0,
});

export default CartContex;