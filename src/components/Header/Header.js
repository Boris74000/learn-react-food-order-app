import React from "react";

import Cart from "./../Cart/Cart";

import classes from './Header.module.css';

const Header = (props) => {
    // console.log(props.dishInCartData);

    const incrementDish = (id) => {
        props.incrementDish(id);
    }

    const decreaseDish = (id) => {
        props.decreaseDish(id);

    }

    return (
        <header>
            <nav className={classes.navbar}>
                <div>
                    <p className={classes.navbar__sitename}>ReactMeals</p>
                </div>
                <div>
                    <Cart
                        dishInCartData={props.dishInCartData}
                        // numberDishesInCart={props.numberDishesInCart}
                        incrementDish={incrementDish}
                        decreaseDish={decreaseDish}
                    />
                </div>
            </nav>
            <div className={classes.banner}>
                <div></div>
            </div>
        </header>
    );
}

export default Header;