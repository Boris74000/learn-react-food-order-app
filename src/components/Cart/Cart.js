import React, { useContext, useState } from "react";

import classes from "./Cart.module.css";
import CartPictogram from "./../../assets/cart-pictogram.svg";
import CartContex from "../../context/cart-context";
import CartModal from "./CartModal";


const Cart = (props) => {
    const context = useContext(CartContex);
    const [isDisplayCart, setIsDisplayCart] = useState(false);

    const displayCart = () => {
        setIsDisplayCart(true);
    };

    const hideCart = () => {
        setIsDisplayCart(false);
    };

    const incrementDish = (id) => {
        props.incrementDish(id);
    }

    const decreaseDish = (id) => {
        props.decreaseDish(id);

    }

    let cartModal = "";

    if (isDisplayCart) {
        cartModal = <CartModal
            onHideCart={hideCart}
            dishInCartData={props.dishInCartData}
            incrementDish={incrementDish}
            decreaseDish={decreaseDish}
        />
    }

    return (
        <div>
            <div>
                {cartModal}
            </div>

            <button className={classes.cart} onClick={displayCart}>
                <img className={classes.cart__pictogram} src={CartPictogram} alt="cart pictogram" />
                <p>Your cart</p>
                <p className={classes.cart__numberDishesInCart}>{context.numberDishesInCart}</p>
            </button>
        </div>
    );
}

export default Cart;