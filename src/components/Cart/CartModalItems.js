import React from "react";

import classes from "./CartModalItems.module.css";

const CartModalItems = (props) => {
    const incrementDish = () => {
        const id = props.id;
        props.incrementDish(id)
    };

    const decreaseDish = () => {
        const id = props.id;
        props.decreaseDish(id);  
    };

    

    return (
        <li className={classes.cartModalItems}>
            <div>
                <h2>{props.name}</h2>
                <div>
                    <p className={classes.cartModalItems__itemPrice}>${props.price}</p>
                    <p className={classes.cartModalItems__amount}>x {props.amountOrder}</p>
                </div>
            </div>
            <div>
                <button className={classes.cartModalItems__remove} onClick={decreaseDish}>-</button>
                <button className={classes.cartModalItems__add} onClick={incrementDish}>+</button>
            </div>
        </li>
    );
}

export default CartModalItems;