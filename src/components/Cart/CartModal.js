import React from "react";

import CartModalItems from "./CartModalItems";
import Card from "./../UI/Card";

import classes from "./CartModal.module.css";


const CartModal = (props) => {
    const dishDataArray = props.dishInCartData;

    const getTotalAmont = () => {
        return dishDataArray.reduce((previousValue, currentValue) => previousValue + currentValue.price * currentValue.amountOrdered, 0).toFixed(2);
    };

    const incrementDish = (id) => {
        props.incrementDish(id);
    }

    const decreaseDish = (id) => {
        props.decreaseDish(id);
    }

    const order = () => {
        console.log("Ordering...");
    }

    return (
        <React.Fragment>
            <div className={classes.overlay} onClick={props.onHideCart}></div>
            <Card className={classes.cartModal}>
                <ul>
                    <React.Fragment>
                        {props.dishInCartData.length > 0
                            ? props.dishInCartData.map(dishData =>

                                <CartModalItems
                                    key={dishData.id}
                                    name={dishData.name}
                                    id={dishData.id}
                                    price={dishData.price}
                                    amountOrder={dishData.amountOrdered}
                                    incrementDish={incrementDish}
                                    decreaseDish={decreaseDish}
                                />
                            )
                            : <li>Cart is empty</li>

                        }

                        {props.dishInCartData.length > 0 &&
                        <div>
                            <div className={classes.totalAmount}>
                                <p>Total Amount</p>
                                <p>${getTotalAmont()}</p>
                            </div>
                            <div className={classes.closeAndOrderContainer}>
                                <button onClick={props.onHideCart}>Close</button>
                                <button onClick={order}>Order</button>
                            </div>

                        </div>
                        }
                    </React.Fragment>
                </ul>
            </Card>
        </React.Fragment>
    );
};

export default CartModal;