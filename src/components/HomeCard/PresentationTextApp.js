import React from "react";

import Card from "./../UI/Card";

import classes from "./PresentationTextApp.module.css";

const PresentationTextApp = () => {
    return (
        <section>
            <Card className={classes.homeCard}>
                <div>
                    <h1>Delicious Food, Delivered To You</h1>
                    <p>Choose your favorite meal from our broad selection of
                        available meals and enjoy a delicious lunch or dinner
                        at home.</p>
                    <p>All our meals are cooked with hight-quality ingredients,
                        just-in-time and of course by experienced chefs !</p>
                </div>
            </Card>
        </section>
    );
}

export default PresentationTextApp;