import React, {useState} from "react";

import Button from "./../UI/Button";

import classes from "./AddDish.module.css";

const AddDish = (props) => {
    const [enteredAmount, setEnteredAmount] = useState("");
    const [itemCount, setItemCount] = React.useState(0);

    const amountHandler = (e) => {
        setEnteredAmount(e.target.value);
    }

    const submitHandlerAddDish = (e) => {
        e.preventDefault();
        setItemCount(itemCount + parseInt(enteredAmount));
    }



    return (
        <form className={classes.addDishForm}
              onSubmit={submitHandlerAddDish}
        >
            <div>
                <label htmlFor="amount">Amount</label>
                <input type="number"
                       id="amount"
                       name="amount"
                       min="0"
                       max="10"
                       value={enteredAmount}
                       onChange={amountHandler}
                />
            </div>
            <Button className={classes.addDishFormSubmit} type="submit">
                + Add
            </Button>
        </form>
    );
}

export default AddDish;