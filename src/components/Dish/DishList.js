import React from "react";

import Card from "./../UI/Card";
// import AddDish from "./AddDish";
import DishListItem from "./DishListItem";

import classes from './DishList.module.css';


const DishList = (props) => {
    const dishAddToCart = (id, dishAmount) => {
        props.onDishAddToCart(id, dishAmount);
    };

    return (
        <section>
            <Card className={classes.dishList}>
                <ul>
                    {props.dishData.map(dish =>
                        // <li className={classes.dishList__item}>
                        //     <div>
                        //         <h2>{dish.name}</h2>
                        //         <p>{dish.description}</p>
                        //         <p className={classes.dishList__itemPrice}>${dish.price}</p>
                        //     </div>
                        //     <div>
                        //         <AddDish/>
                        //     </div>
                        // </li>
                        <DishListItem
                            key={dish.id}
                            id={dish.id}
                            name={dish.name}
                            description={dish.description}
                            price={dish.price}
                            onDishAddToCart={dishAddToCart}
                        />
                    )}
                </ul>
            </Card>
        </section>
    );
}

export default DishList;