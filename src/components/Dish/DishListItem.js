import React, { useState } from "react";

import classes from "./DishListItem.module.css";



const DishListItem = (props) => {
    const [dishAmount, setDishAmount] = useState(1);

    // const addToCartHandler = () => {
    //     const id = props.id;
    //     props.onDishAddToCart(id);
    // };

    const dishAmountChangeHandler = (e) => {
        setDishAmount(e.target.value);
    };

    const formDishAmountHandler = (e) => {
        e.preventDefault();
        const id = props.id;
        props.onDishAddToCart(id, Number(dishAmount));
    };

    return (
        <li className={classes.dishList__item}>
            <div>
                <h2>{props.name}</h2>
                <p>{props.description}</p>
                <p className={classes.dishList__itemPrice}>${props.price}</p>
            </div>
           
            <form onSubmit={formDishAmountHandler} className={classes.dishList__itemForm}>
                <div>
                    <label htmlFor="dishAmount">Amount</label>
                    <input type="number" id="dishAmount" name="dishAmount" onChange={dishAmountChangeHandler}
                        min="1" max="10" defaultValue={"1"} required />
                </div>
                <div>
                    <button type='submit'>+ Add</button>
                </div>

            </form>
        </li>
    );
};

export default DishListItem;